function F_2 = obliczF2(h_1)
    global ALPHA_1
    F_2 = ALPHA_1 * sqrt(h_1);
end