function [punkt_pracy] = podzielNaZbioryRozmyte(liczba, min, max)
    global PUNKT_PRACY_F_D
    global ALPHA_1
    global ALPHA_2
    
if liczba == 0
    error('Zwieksz liczbe zbiorow rozmytych');
end

delta=(max-min)/liczba;
for iter = 1:liczba
    punkt_pracy(iter).h_2 = min + delta/2 + delta*(iter-1);
    punkt_pracy(iter).h_1 = (ALPHA_2 ^ 2 * punkt_pracy(iter).h_2) / (ALPHA_1 ^ 2);
    punkt_pracy(iter).F_D = PUNKT_PRACY_F_D;
    punkt_pracy(iter).F_1 = ALPHA_1 * sqrt(punkt_pracy(iter).h_1) - punkt_pracy(iter).F_D;
end

end

