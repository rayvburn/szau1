function F_3 = obliczF3(h_2)
    global ALPHA_2
    F_3 = ALPHA_2 * sqrt(h_2);
end