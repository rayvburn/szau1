%% Warunki początkowe (w punkcie pracy)
% Wartości znane na tym etapie: `F_1`, `F_d`, `h_2`
%
% suma dopływów
F_dop = PUNKT_PRACY_F_1 + PUNKT_PRACY_F_D;

% wysokość cieczy w zbiorniku nr 1 w punkcie pracy - z równania na V1
h_1 = F_dop^2 / ALPHA_1^2;
% wysokość cieczy w zbiorniku nr 2 w punkcie pracy - z równania na V2
h_2 = F_dop^2 / ALPHA_2^2;

% odpływ ze zbiornika nr 1 w punkcie pracy
F_2 = obliczF2(h_1);

% odpływ ze zbiornika nr 2 w punkcie pracy
F_3 = obliczF3(h_2);

save('warunki_poczatkowe.mat', 'F_dop', 'h_1', 'h_2', 'F_2', 'F_3', '-nocompression')