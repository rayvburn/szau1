function [nowe_delta_u] = ograniczeniePrzyrostuSterowania(delta_u, delta_u_max)
if abs(delta_u) > abs(delta_u_max)
    nowe_delta_u = abs(delta_u_max) * sign(delta_u);
else
    nowe_delta_u = delta_u;
end
end

