clear all;

%% Stałe związane z eksperymentem
load('stale.mat');
%% Warunki początkowe (w punkcie pracy)
load('warunki_poczatkowe.mat');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Konfiguracja przebiegu eksperymentu
% okres próbkowania [s] taki jak przy zbieraniu odpowiedzi skokowej
% (wczytany z pliku `odpowiedz_skokowa.mat`)
global TP
global U_MAX
global DELTA_U_MAX
% okres próbkowania [s], uwaga: (TP <= TAU / 2)
TP = 150;
% długość eksperymentu [s]
DLUGOSC = TP * 10000;
% ograniczenie sterowań
U_MAX = 150;
DELTA_U_MAX = 5;

%% Odpowiedź skokowa układu
S = generujOdpowiedzSkokowa(obliczH1zH2(PUNKT_PRACY_H_2), PUNKT_PRACY_H_2, 10, TP, 25000, 0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Przygotowanie wektorów wartości zadanych układu.
y_zad_wektor = przygotujWektorWartosciZadanych(DLUGOSC / TP, ...
    [0.3, 0.4, 0.5, 0.7, 0.9], ...
    [PUNKT_PRACY_H_2, ...
     PUNKT_PRACY_H_2 + 2, ...
     PUNKT_PRACY_H_2 - 2, ...
     PUNKT_PRACY_H_2 + 15, ...
     PUNKT_PRACY_H_2 - 15, ...
     PUNKT_PRACY_H_2 + 25]);
F_D = PUNKT_PRACY_F_D;
N_PROBKI = length(y_zad_wektor);

%% Wykorzystując dotychczasowe informacje (stałe, warunki początkowe) 
%% możliwe jest wyznaczenie wartości objętości w zbiornikach nr 1 i 2. 
fprintf("Początkowe wysokości cieczy w zbiornikach to: h_1: %3.5f, h_2: %3.5f\n", h_1, h_2);
% Wartości wyjściowe licząc od 3-ciej dotyczą zmiennych wykorzystywanych 
% przy linearyzacji.
% zmienne *_lin0 są stałe przez cały czas trwania eksperymentu;
% można zignorować nieużywane wartości wyjściowe funkcji przez `~`
[V_1, V_2, V_1_lin, V_1_lin0, V_2_lin, V_2_lin0, h_1_lin, h_1_lin0, h_2_lin, h_2_lin0] = ...
    inicjalizujZmienneProcesu(h_1, h_2);

%% Algorytm DMC
% Długość wektora `S` określa liczbę wierszy macierzy dynamicznej `M`,
% natomiast wybrany horyzont sterowania wpływa na liczbę jej kolumn.
N = length(S);
% horyzont sterowania
Nu = N;
% horyzont predykcji
D = length(S);
% współczynnik kary za przyrosty sterowania
LAMBDA = 0.1;

%% Przygotowanie macierzy dynamicznej
[M, MP, K] = utworzMacierzeDMC(S, N, Nu, D, LAMBDA);

%% Przygotowanie wektora przyrostów sterowań
% przechowuje wartości przeszłych sterowań
for i = 1:D-1
   delta_u_p(i) = 0;
end
% Początkowe wymuszenie i jego przyrost
u_k = PUNKT_PRACY_F_1;
delta_u = 0;
% Początkowa wartość wyjścia
y_k = PUNKT_PRACY_H_2;

% wymuszenia układu
F_1_in = ones(1, N_PROBKI) * PUNKT_PRACY_F_1;

%% Symulacja działania układu - modelu nieliniowego oraz zlinearyzowanego
for i = 1 : N_PROBKI
    %% Zmiana wartości zadanej
    y_zad = y_zad_wektor(i);

    %% Model zlinearyzowany
    [V_1_lin, V_2_lin, h_1_lin, y_k] = obiektZlinearyzowany(V_1_lin, ...
        F_1_in(i), F_D, h_1_lin, h_1_lin0, V_2_lin, V_2_lin0, h_2_lin, h_2_lin0);
    
    %% Regulator
    e_k = y_zad - y_k;
    delta_u_k = sterowanieDMC(K, y_zad, y_k, MP, delta_u_p);
    delta_u_p = przesunDeltaU(D, delta_u_p, delta_u_k);

    % obliczenie nowego sterowania na podstawie poprzedniego oraz `delty`
    delta_u_p(1) = ograniczeniePrzyrostuSterowania(delta_u_p(1), DELTA_U_MAX);
    u_k = u_k + delta_u_p(1);  
    
    % ograniczenie wartosci sterowania
    u_k(1) = ograniczenieSterowania(u_k(1), U_MAX);
    
    F_1_in = zastosujOpoznienieWymuszeniaUkladu(F_1_in, i, u_k(1), TP);
   
    %% Dane do wykresów oraz aktualizacja wysokości słupa cieczy
    % macierze zmieniające swój rozmiar w kolejnych iteracjach
    V_1_wektor(i) = V_1;
    V_1_lin_wektor(i) = V_1_lin;
    h_1_wektor(i) = h_1;
    h_1_lin_wektor(i) = h_1_lin;
    V_2_wektor(i) = V_2;
    V_2_lin_wektor(i) = V_2_lin;
    h_2_wektor(i) = h_2;
    h_2_lin_wektor(i) = y_k; % h_2_lin;
    u_k_wektor(i) = u_k(1);
    uchyb_wektor(i) = e_k;
    czas(i) = (i-1) * TP;
end

%% Wysokość słupa wody w zbiorniku nr 2
figure;
title('DMC - wysokość słupa wody w zbiorniku nr 2')
plot(czas, h_2_lin_wektor, 'r');
hold on
plot(czas, y_zad_wektor, 'g--');
hold on
% yyaxis right % sterowania na osi po prawej
plot(czas, u_k_wektor, 'm--');
grid on
plot(czas, F_1_in(1:length(czas)), 'b--');
grid on
ylabel('Dopływ [cm^3/s]');
legend('h_2 lin', 'y_{zad}', 'F_1', 'Location', 'best', 'NumColumns', 2);

%% Wysokość słupa wody w zbiorniku nr 2 (bez sterowań)
figure;
title('DMC - wysokość słupa wody w zbiorniku nr 2')
plot(czas, h_2_lin_wektor, 'r');
hold on
plot(czas, y_zad_wektor, 'g--');
grid on
legend('h_2 lin', 'y_{zad}', 'Location', 'best', 'NumColumns', 2);