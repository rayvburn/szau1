function [V_1_lokalne, V_2_lokalne, h_1_lokalne, h_2_lokalne] = obiektNieliniowy(V_1, F_1, F_D, h_1, V_2, h_2)
    %% Model nieliniowy
    % wyliczenie dopływów dla modelu nieliniowego. W tej iteracji używamy 
    % uprzednio wyliczonych `h_1` oraz `h_2`;
    % nie ma potrzeby zapisywać poniższych wartości do wyświetlenia na
    % wykresach
    F_2 = obliczF2(h_1);
    F_3 = obliczF3(h_2);
    
    % numeryczne rozwiązanie równań różniczkowych *nieliniowych* metodą
    % punktu środkowego (Eulera);
    % zaktualizowana objętość cieczy w zbiorniku nr 1
    V_1_lokalne = obliczV1Nlin(V_1, F_1, F_D, h_1);
    % zaktualizowana objętość cieczy w zbiorniku nr 2
    V_2_lokalne = obliczV2Nlin(V_2, F_2, h_2);
    
    h_1_lokalne = obliczH1(V_1_lokalne);
    h_2_lokalne = obliczH2(V_2_lokalne);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Funkcje pomocnicze - model nieliniowy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Funkcja przejścia dla równania związanego ze zbiornikiem nr 1
function fV1 = funkcjaV1Nlin(F_1, F_D, h_1)
    global ALPHA_1
    fV1 = F_1 + F_D - ALPHA_1 * sqrt(h_1);
end

%% Funkcja przejścia dla równania związanego ze zbiornikiem nr 2
function fV2 = funkcjaV2Nlin(F_2, h_2)
    global ALPHA_2
    fV2 = F_2 - ALPHA_2 * sqrt(h_2);
end

%% Euler Midpoint method
function V_1_out = obliczV1Nlin(V_1, F_1, F_D, h_1)
    global TP
    V_1_pom = V_1 + 0.5 * TP * funkcjaV1Nlin(F_1, F_D, h_1);
    h_1_pom = obliczH1(V_1_pom);
    
    dV_1 = TP * funkcjaV1Nlin(F_1, F_D, h_1_pom);
    V_1_out = V_1 + dV_1;
end

%% Euler Midpoint method
function V_2_out = obliczV2Nlin(V_2, F_2, h_2)
    global TP
    V_2_pom = V_2 + 0.5 * TP * funkcjaV2Nlin(F_2, h_2);
    h_2_pom = obliczH2(V_2_pom);

    dV_2 = TP * funkcjaV2Nlin(F_2, h_2_pom);
    V_2_out = V_2 + dV_2;
end