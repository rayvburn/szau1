function [S] = generujOdpowiedzSkokowa(h_1, h_2, amplituda_skoku, t_probkowania, liczba_probek, czy_rysowac)
%GENERUJODPOWIEDZSKOKOWA Summary of this function goes here
%   h_1 punkt linearyzacji
%   h_2 punkt linearyzacji

    %% Stałe związane z eksperymentem
    load('stale.mat', 'ALPHA_1', 'ALPHA_2', 'A_1', 'C_2', 'TAU', 'PUNKT_PRACY_F_D');
    
    global TP
    %% Obsługa błędów
    % Sprawdz, czy wartosc TP (okres probkowania) jest w workspace
    if exist('TP', 'var') == 0
        TP = t_probkowania;
        warning('`generujOdpowiedzSkokowa` - Utworzona stała globalna TP o wartości parametru `t_probkowania`: %4.5f\n', TP);
        % funkcje w `obiektZlinearyzowany` odczytują TP jako parametr globalny
    elseif TP ~= t_probkowania
        error('`generujOdpowiedzSkokowa` - Argument `t_probkowania` w `generujOdpowiedzSkokowa` nie jest równy globalnej stałej TP!');
    end
    
    %% Obliczanie parametrów linearyzowanego punktu pracy
    pkt_pracy.h_1 = h_1;
    pkt_pracy.h_2 = h_2;
    pkt_pracy.F_1 = obliczF1(h_1, PUNKT_PRACY_F_D);
    pkt_pracy.F_D = PUNKT_PRACY_F_D;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Konfiguracja przebiegu eksperymentu
    % długość eksperymentu [s]
    DLUGOSC = t_probkowania * liczba_probek;
    % moment zaaplikowania na układ wymuszenia w postaci skoku [s];
    % korekta w postaci TAU (opóźnienia układu), aby uniknąć niepoprawnych wartości
    % wymuszeń na starcie
    CZAS_WYMUSZENIA = TAU + (DLUGOSC * 0.1);
    % amplituda wymuszenia - skoku [cm^3/s] (wartość nie ma znaczenia, kiedy
    % wyświetlane są przebiegi dla regulatorów)
    AMPLITUDA = pkt_pracy.F_1 + amplituda_skoku;

    %% Przygotowanie wektorów sterowań układu.
    [F_1_in_zad, F_1_in] = przygotujWektorSterowanUkladuOtwartego(pkt_pracy.F_1, ...
        AMPLITUDA, CZAS_WYMUSZENIA, TAU, DLUGOSC, t_probkowania);
    F_D = PUNKT_PRACY_F_D;
    N_PROBKI = size(F_1_in, 2);

    %% Wykorzystując dotychczasowe informacje (stałe, warunki początkowe) 
    %% możliwe jest wyznaczenie wartości objętości w zbiornikach nr 1 i 2. 
    fprintf("`generujOdpowiedzSkokowa` - Początkowe wysokości cieczy w zbiornikach to: h_1: %3.5f, h_2: %3.5f\n", h_1, h_2);
    % Wartości wyjściowe licząc od 3-ciej dotyczą zmiennych wykorzystywanych 
    % przy linearyzacji.
    % zmienne *_lin0 są stałe przez cały czas trwania eksperymentu;
    % można zignorować nieużywane wartości wyjściowe funkcji przez `~`
    [V_1, V_2, V_1_lin, V_1_lin0, V_2_lin, V_2_lin0, h_1_lin, h_1_lin0, h_2_lin, h_2_lin0] = ...
        inicjalizujZmienneProcesu(pkt_pracy.h_1, pkt_pracy.h_2);

    %% Odpowiedź skokowa 
    S = zeros(1, N_PROBKI);
    U = zeros(1, N_PROBKI);
    if AMPLITUDA == 0
        disp("Niepoprawna wartość amplitudy")
        return
    end
    DZIELNIK_WSPOLCZYNNIKOW_ODPOWIEDZI = AMPLITUDA - pkt_pracy.F_1;

    % znalezienie numeru próbki skojarzonej z początkiem wymuszenia w postaci
    % skoku jednostkowego
    PROBKA_SKOKU = -1;
    for i = 1 : N_PROBKI
        if F_1_in_zad(i) ~= pkt_pracy.F_1
            PROBKA_SKOKU = i;
            break;
        end
    end

    %% Symulacja działania układu - modelu nieliniowego oraz zlinearyzowanego
    for i = 1 : N_PROBKI
        %% Wybór sterowania z uprzednio przygotowanego wektora
        % który uwzględnia opóźnienie układu)
        F_1 = F_1_in(1, i);

        %% Model nieliniowy
        [V_1, V_2, h_1, h_2] = obiektNieliniowy(V_1, F_1, F_D, h_1, V_2, h_2);

        %% Model zlinearyzowany
        [V_1_lin, V_2_lin, h_1_lin, h_2_lin] = obiektZlinearyzowany(V_1_lin, ...
            F_1, F_D, h_1_lin, h_1_lin0, V_2_lin, V_2_lin0, h_2_lin, h_2_lin0);

        %% Dane do zebrania odpowiedzi skokowej
        S(i) = h_2_lin;
        U(i) = F_1;

        %% Dane do wykresów oraz aktualizacja wysokości słupa cieczy
        % macierze zmieniające swój rozmiar w kolejnych iteracjach
        V_1_wektor(i) = V_1;
        V_1_lin_wektor(i) = V_1_lin;
        h_1_wektor(i) = h_1;
        h_1_lin_wektor(i) = h_1_lin;
        V_2_wektor(i) = V_2;
        V_2_lin_wektor(i) = V_2_lin;
        h_2_wektor(i) = h_2;
        h_2_lin_wektor(i) = h_2_lin;
        czas(i) = (i-1) * t_probkowania;
    end

    if czy_rysowac
        %% Wysokość słupa wody w zbiorniku nr 2
        figure;
        plot(czas, h_2_wektor, 'b');
        hold on
        plot(czas, h_2_lin_wektor, 'r');
        ylabel('Wysokość poziomu cieczy [cm]')
        yyaxis right
        hold on
        plot(czas, F_1_in, 'm--');
        xlabel('czas [s]');
        ylabel('Dopływ [cm^3/s]');
        legend('h_2 model nieliniowy', 'h_2 model zlinearyzowany', ...
            'Suma dopływów', 'Location', 'east', 'NumColumns', 2);
    end

    %% Normalizacja odpowiedzi skokowej
    S = (S - pkt_pracy.h_2) ./ DZIELNIK_WSPOLCZYNNIKOW_ODPOWIEDZI;
    U = (U - pkt_pracy.F_1) ./ DZIELNIK_WSPOLCZYNNIKOW_ODPOWIEDZI;

    % przesunięcie całej sekwencji odpowiedzi o odpowiednią liczbę próbek -
    % uwzględnienie momentu wymuszenia w przebiegach
    S = S(PROBKA_SKOKU:end);
    U = U(PROBKA_SKOKU:end);

    % wyznaczenie długości wektora, uwzględniając wartości spełniające warunek
    D = size(S(S < (0.98 * S(end))), 2);

    fprintf("Odpowiedź ustaliła się po D = %d próbkach przy okresie próbkowania TP = %5.1f [s].\n", D, TP);
    fprintf("Maksymalna znormalizowana wartość wyjścia to: %3.8f.\n\n", S(end));

    % obcięcie niepotrzebnych wartości (związanych ze stanem ustalonym)
    S = S(1:D);

end

