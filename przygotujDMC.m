function [DMC] = przygotujDMC(h_2_pkt_pracy, N, Nu, lambda, F_1_start)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    global TP
    %% Odpowiedź skokowa modeli lokalnych
    DMC.S = generujOdpowiedzSkokowa(obliczH1zH2(h_2_pkt_pracy), h_2_pkt_pracy, 10, TP, 25000, 0);
    
    %% Algorytm DMC
    % Długość wektora `S` określa liczbę wierszy macierzy dynamicznej `M`,
    % natomiast wybrany horyzont sterowania wpływa na liczbę jej kolumn.
    if isinf(N)
        DMC.N = length(DMC.S);
    else    
        DMC.N = N;
    end
    % horyzont sterowania
    if isinf(N)
        DMC.Nu = length(DMC.S);
    else    
        DMC.Nu = Nu;
    end
    % horyzont predykcji
    DMC.D = DMC.N;
    % współczynnik kary za przyrosty sterowania
    DMC.LAMBDA = lambda;

    %% Przygotowanie macierzy dynamicznej
    [DMC.M, DMC.MP, DMC.K] = utworzMacierzeDMC(DMC.S, DMC.N, DMC.Nu, DMC.D, DMC.LAMBDA);

    %% Przygotowanie wektora przyrostów sterowań
    % przechowuje wartości przeszłych sterowań
    for i = 1:DMC.D-1
       DMC.delta_u_p(i) = 0;
    end
    % Początkowe wymuszenie i jego przyrost
    DMC.u_k = F_1_start;
    DMC.delta_u = 0;
end