clear all;

%% Stałe związane z eksperymentem
load('stale.mat');
%% Warunki początkowe (w punkcie pracy)
load('warunki_poczatkowe.mat');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Konfiguracja przebiegu eksperymentu
global TP
% okres próbkowania [s], uwaga: (TP <= TAU / 2)
TP = 30;
% długość eksperymentu [s]
DLUGOSC = TP * 4000;
% moment zaaplikowania na układ wymuszenia w postaci skoku [s];
% korekta w postaci TAU (opóźnienia układu), aby uniknąć niepoprawnych wartości
% wymuszeń na starcie
CZAS_WYMUSZENIA = TAU + (DLUGOSC * 0.1);
% amplituda wymuszenia - skoku [cm^3/s] (wartość nie ma znaczenia, kiedy
% wyświetlane są przebiegi dla regulatorów)
AMPLITUDA = PUNKT_PRACY_F_1 + 7;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Przygotowanie wektorów sterowań układu.
[F_1_in_zad, F_1_in] = przygotujWektorSterowanUkladuOtwartego(PUNKT_PRACY_F_1, ...
    AMPLITUDA, CZAS_WYMUSZENIA, TAU, DLUGOSC, TP);
F_D = PUNKT_PRACY_F_D;
N_PROBKI = size(F_1_in, 2);

%% Wykorzystując dotychczasowe informacje (stałe, warunki początkowe) 
%% możliwe jest wyznaczenie wartości objętości w zbiornikach nr 1 i 2. 
fprintf("Początkowe wysokości cieczy w zbiornikach to: h_1: %3.5f, h_2: %3.5f\n", h_1, h_2);
% Wartości wyjściowe licząc od 3-ciej dotyczą zmiennych wykorzystywanych 
% przy linearyzacji.
% zmienne *_lin0 są stałe przez cały czas trwania eksperymentu;
% można zignorować nieużywane wartości wyjściowe funkcji przez `~`
[V_1, V_2, V_1_lin, V_1_lin0, V_2_lin, V_2_lin0, h_1_lin, h_1_lin0, h_2_lin, h_2_lin0] = ...
    inicjalizujZmienneProcesu(h_1, h_2);

%% Symulacja działania układu - modelu nieliniowego oraz zlinearyzowanego
for i = 1 : N_PROBKI
    %% Wybór sterowania z uprzednio przygotowanego wektora
    % który uwzględnia opóźnienie układu)
    F_1 = F_1_in(1, i);
           
    %% Model nieliniowy
    [V_1, V_2, h_1, h_2] = obiektNieliniowy(V_1, F_1, F_D, h_1, V_2, h_2);
   
    %% Dane do wykresów oraz aktualizacja wysokości słupa cieczy
    % macierze zmieniające swój rozmiar w kolejnych iteracjach
    V_1_wektor(i) = V_1;
    h_1_wektor(i) = h_1;
    V_2_wektor(i) = V_2;
    h_2_wektor(i) = h_2;
    czas(i) = (i-1) * TP;
end

figure('Renderer', 'painters', 'Position', [10 10 1500 900])
subplot(3,2,1)
%% Przebiegi sygnałów
title({'Wykres objetosci cieczy w zbiorniku od czasu';['dla zmiany F_1 o ',num2str(AMPLITUDA - PUNKT_PRACY_F_1)]})
plot(czas, h_1_wektor, 'g')
hold on
plot(czas, h_2_wektor, 'b')
ylabel('Wysokość poziomu cieczy [cm]')
yyaxis right
hold on
plot(czas, F_1_in_zad, ['--', 'r'])
hold on
plot(czas, F_1_in + F_D, ['-', 'r'])
xlabel('czas [s]');
ylabel('Dopływ [cm^3/s]');
grid on
legend('h_1', 'h_2', ...
    'F_{1in}_{zad}', 'Suma dopływów', ...
    'Location', 'east', 'NumColumns', 2);
mkdir('zad1_nielin');
saveas(gcf, [pwd '/zad1_nielin/wysokosci-doplywy.png']);

%% Objętość zbiorników
figure;
title('Objętość zbiorników nr 1 i nr 2');
plot(czas, V_1_wektor, 'g');
hold on
plot(czas, V_2_wektor, 'm');
ylabel('Objętośc [cm^3]')
yyaxis right
hold on
plot(czas, F_1_in_zad, ['--', 'r'])
hold on
plot(czas, F_1_in + F_D, ['-', 'r'])

ylabel('Dopływ [cm^3/s]')
grid on
legend('V_1 n-liniowy',  'V_2 n-liniowy', ...
    'F_{1in}_{zad}', 'Suma dopływów', ...
    'Location', 'east', 'NumColumns', 2);
saveas(gcf, [pwd '/zad1_nielin/objetosci-doplywy.png']);