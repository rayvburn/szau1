close all;
clear all;

%% Stałe związane z eksperymentem
load('stale.mat');
%% Warunki początkowe (w punkcie pracy)
load('warunki_poczatkowe.mat');

global TP
% okres próbkowania [s], uwaga: (TP <= TAU / 2)
TP = 150;

% generowanie skoku
[S] = generujOdpowiedzSkokowa(obliczH1zH2(PUNKT_PRACY_H_2), PUNKT_PRACY_H_2, 10, TP, 25000, 1);

mkdir('zad1_odp_skok');
saveas(gcf, [pwd '/zad1_odp_skok/h2-doplywy.png']);