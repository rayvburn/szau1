function [F_1] = obliczF1(h_1, F_D)
%OBLICZF1 Summary of this function goes here
%   Detailed explanation goes here
    global ALPHA_1
    F_1 = ALPHA_1 * sqrt(h_1) - F_D;
end