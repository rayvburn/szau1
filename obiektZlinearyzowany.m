function [V_1_lin_lokalne, V_2_lin_lokalne, h_1_lin_lokalne, h_2_lin_lokalne] = obiektZlinearyzowany(V_1, F_1, F_D, h_1, h_10, V_2, V_20, h_2, h_20)
    %% Model zlinearyzowany
    % Wyliczenie dopływów dla modelu zlinearyzowanego. W tej iteracji 
    % używamy uprzednio wyliczonych `h_1` oraz `h_2`
    
    % zaktualizowana objętość i wysokość cieczy w zbiorniku nr 1
    V_1_lin_lokalne = obliczV1Lin(V_1, F_1, F_D, h_1, h_10);
    % zaktualizowana objętość i wysokość cieczy w zbiorniku nr 2
    V_2_lin_lokalne = obliczV2Lin(V_2, V_20, h_1, h_10, h_2, h_20);
    
    h_1_lin_lokalne = obliczH1(V_1_lin_lokalne);
    h_2_lin_lokalne = obliczH2Lin(V_2_lin_lokalne, V_20);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Funkcje pomocnicze - model zlinearyzowany
%%
%% Funkcja przejścia dla równania zlinearyzowanego
%%% TODO: opis
function Lin = funkcjaRozniczka(wsp_licznik, h_lin, h_lin0)
    Lin = wsp_licznik * sqrt(h_lin0) + wsp_licznik / (2 * sqrt(h_lin0)) * (h_lin - h_lin0);
end

%% Euler Midpoint method
function V_1_lin = obliczV1Lin(V_1_lin, F_1, F_D, h_1_lin, h_1_lin0)
    global TP
    global ALPHA_1

    V_1_lin_pom = V_1_lin + F_1 + F_D - funkcjaRozniczka(ALPHA_1, h_1_lin, h_1_lin0);
    h_1_lin_pom = obliczH1(V_1_lin_pom);

    dV_1_lin = TP * (F_1 + F_D - funkcjaRozniczka(ALPHA_1, h_1_lin_pom, h_1_lin0));
    V_1_lin = V_1_lin + dV_1_lin;
end

%% Funkcja przejścia dla równania zlinearyzowanego związanego 
%% ze zbiornikiem nr 2
function V_2_lin = obliczV2Lin(V_2_lin, V_2_lin0, h_1_lin, h_1_lin0, h_2_lin, h_2_lin0)
    global TP
    global ALPHA_1
    global ALPHA_2
    
    V_2_lin_pom = V_2_lin + funkcjaRozniczka(ALPHA_1, h_1_lin, h_1_lin0) - funkcjaRozniczka(ALPHA_2, h_2_lin, h_2_lin0);
    h_2_lin_pom = obliczH2Lin(V_2_lin_pom, V_2_lin0);

    dV_2_lin = TP * (funkcjaRozniczka(ALPHA_1, h_1_lin, h_1_lin0) - funkcjaRozniczka(ALPHA_2, h_2_lin_pom, h_2_lin0));
    V_2_lin = V_2_lin + dV_2_lin;
end