function h_2 = obliczH2(V_2)
    global C_2
    h_2 = power(V_2 / C_2, 1/3);
end