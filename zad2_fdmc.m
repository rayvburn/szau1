clear all;

%% Stałe związane z eksperymentem
load('stale.mat');
%% Warunki początkowe (w punkcie pracy)
load('warunki_poczatkowe.mat');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Konfiguracja przebiegu eksperymentu
% okres próbkowania [s] taki jak przy zbieraniu odpowiedzi skokowej
% (wczytany z pliku `odpowiedz_skokowa.mat`)
global TP
global U_MAX
global DELTA_U_MAX
% okres próbkowania [s], uwaga: (TP <= TAU / 2)
TP = 150;
% długość eksperymentu [s]
DLUGOSC = TP * 10000;
% ograniczenie sterowań
U_MAX = 150;
DELTA_U_MAX = 5;

%% Podział zmiennej `h_2` na kilka zbiorów rozmytych
TS_LICZBA_ZBIOROW = 7;
TS_ZAKRES_H2 = [PUNKT_PRACY_H_2 - 25; PUNKT_PRACY_H_2 + 25];
TS_NACHYLENIE_TRAP = 0.2 * ones(TS_LICZBA_ZBIOROW-1,1);
ts_punkty_pracy = podzielNaZbioryRozmyte(TS_LICZBA_ZBIOROW, TS_ZAKRES_H2(1), TS_ZAKRES_H2(2));

% Wyliczenie wartości początkowych do modelu zlinearyzowanego
[V_1, V_2, V_1_lin, V_1_lin0, V_2_lin, V_2_lin0, h_1_lin, h_1_lin0, h_2_lin, h_2_lin0] = inicjalizujZmienneProcesu(h_1, h_2);

% Wartości początkowe dla modelu pomocniczego F
[~, ~, V_1_f, ~, V_2_f, V_2_f_0, h_1_f, h_1_f_0, h_2_f, h_2_f_0] = inicjalizujZmienneProcesu(h_1, h_2);
% Wartości początkowe dla modelu wynikowego TS
[~, ~, V_1_ts, ~, V_2_ts, V_2_ts_0, h_1_ts, h_1_ts_0, h_2_ts, h_2_ts_0] = inicjalizujZmienneProcesu(h_1, h_2);

% Wyliczenie wartości początkowych dla modeli lokalnych Takagi Sugeno
[V_1_fuzzy, V_2_fuzzy, V_1_lin_fuzzy, V_1_lin0_fuzzy, V_2_lin_fuzzy, V_2_lin0_fuzzy, h_1_lin_fuzzy, h_1_lin0_fuzzy, h_2_lin_fuzzy, h_2_lin0_fuzzy] ...
    = inicjalizujZmienneProcesuTS(ts_punkty_pracy, V_1, V_2, h_1, h_2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Przygotowanie wektorów wartości zadanych układu.
y_zad_wektor = przygotujWektorWartosciZadanych(DLUGOSC / TP, ...
    [0.3, 0.4, 0.5, 0.7, 0.9], ...
    [PUNKT_PRACY_H_2, ...
     PUNKT_PRACY_H_2 + 2, ...
     PUNKT_PRACY_H_2 - 2, ...
     PUNKT_PRACY_H_2 + 15, ...
     PUNKT_PRACY_H_2 - 15, ...
     PUNKT_PRACY_H_2 + 25]);
F_D = PUNKT_PRACY_F_D;
N_PROBKI = length(y_zad_wektor);

%% Wykorzystując dotychczasowe informacje (stałe, warunki początkowe) 
%% możliwe jest wyznaczenie wartości objętości w zbiornikach nr 1 i 2. 
fprintf("Początkowe wysokości cieczy w zbiornikach to: h_1: %3.5f, h_2: %3.5f\n", h_1, h_2);
% Wartości wyjściowe licząc od 3-ciej dotyczą zmiennych wykorzystywanych 
% przy linearyzacji.
% zmienne *_lin0 są stałe przez cały czas trwania eksperymentu;
% można zignorować nieużywane wartości wyjściowe funkcji przez `~`
[V_1, V_2, V_1_lin, V_1_lin0, V_2_lin, V_2_lin0, h_1_lin, h_1_lin0, h_2_lin, h_2_lin0] = ...
    inicjalizujZmienneProcesu(h_1, h_2);

%% Algorytm FDMC
% DMC:
% - Odpowiedź skokowa modeli lokalnych
% - przygotowanie macierzy dynamicznej
% - Przygotowanie wektora przyrostów sterowań
% - Początkowe wymuszenie i jego przyrost
for i = 1 : TS_LICZBA_ZBIOROW
    DMC_lokalne(i) = przygotujDMC(ts_punkty_pracy(i).h_2, Inf, Inf, 0.001, PUNKT_PRACY_F_1);
end
% model "podstawowy"
DMC_lin = przygotujDMC(PUNKT_PRACY_H_2, Inf, Inf, 0.1, PUNKT_PRACY_F_1);
% wartość początkowa sterowania dla modelu Takagi-Sugeno
u_k_ts = PUNKT_PRACY_F_1;
% i `F`
u_k_f = u_k_ts;

% Początkowa wartość wyjścia
y_k_lin = PUNKT_PRACY_H_2;
y_k_ts = PUNKT_PRACY_H_2;
%poczatkowa wartosc h_2
h_2_wektor = PUNKT_PRACY_H_2;

% wymuszenia układu
F_1_in_lin = ones(1, N_PROBKI) * PUNKT_PRACY_F_1;
F_1_in_ts = ones(1, N_PROBKI) * PUNKT_PRACY_F_1;

%% Symulacja działania układu - modelu nieliniowego oraz zlinearyzowanego
for i = 1 : N_PROBKI
    %% Zmiana wartości zadanej
    y_zad = y_zad_wektor(i);

    %% Model zlinearyzowany
    [V_1_lin, V_2_lin, h_1_lin, h_2_lin] = obiektZlinearyzowany(V_1_lin, ...
        F_1_in_lin(i), F_D, h_1_lin, h_1_lin0, V_2_lin, V_2_lin0, h_2_lin, h_2_lin0);
    %% Regulator DMC nierozmyty
    y_k_lin = h_2_lin;
    e_k_lin = y_zad - y_k_lin;
    delta_u_k = sterowanieDMC(DMC_lin.K, y_zad, y_k_lin, DMC_lin.MP, DMC_lin.delta_u_p);
    delta_u_k = ograniczeniePrzyrostuSterowania(delta_u_k, DELTA_U_MAX);
    DMC_lin.delta_u_p = przesunDeltaU(DMC_lin.D, DMC_lin.delta_u_p, delta_u_k);
    % obliczenie nowego sterowania na podstawie poprzedniego oraz `delty`
    DMC_lin.u_k = DMC_lin.u_k + DMC_lin.delta_u_p(1);
    % ograniczenie wartosci sterowania
    DMC_lin.u_k = ograniczenieSterowania(DMC_lin.u_k, U_MAX);
    % uwzględnienie TAU układu
    F_1_in_lin = zastosujOpoznienieWymuszeniaUkladu(F_1_in_lin, i, DMC_lin.u_k(1), TP);
    
    %% FDMC
    w = Wyznacz_wartosci_funkcji_przynaleznosci(TS_LICZBA_ZBIOROW, TS_ZAKRES_H2(1), TS_ZAKRES_H2(2), h_2_ts, TS_NACHYLENIE_TRAP);
    V_1_ts_sum = 0; V_2_ts_sum = 0; h_1_ts_sum = 0; h_2_ts_sum = 0;
    for j = 1 : TS_LICZBA_ZBIOROW
        [V_1_lin_fuzzy(j), V_2_lin_fuzzy(j), h_1_lin_fuzzy(j), h_2_lin_fuzzy(j)] = obiektZlinearyzowany(V_1_lin_fuzzy(j), ...
            u_k_ts, F_D, h_1_lin_fuzzy(j), h_1_lin0_fuzzy(j), V_2_lin_fuzzy(j), V_2_lin0_fuzzy(j), h_2_lin_fuzzy(j), h_2_lin0_fuzzy(j));
        V_1_ts_sum = V_1_ts_sum + w(j) * V_1_lin_fuzzy(j);
        V_2_ts_sum = V_2_ts_sum + w(j) * V_2_lin_fuzzy(j);
        h_1_ts_sum = h_1_ts_sum + w(j) * h_1_lin_fuzzy(j);
        h_2_ts_sum = h_2_ts_sum + w(j) * h_2_lin_fuzzy(j);
    end
    h_2_ts = h_2_ts_sum;
    h_1_ts = h_1_ts_sum;
    V_1_ts = V_1_ts_sum;
    V_2_ts = V_2_ts_sum;

    %% Sterowania regulatorów lokalnych
    for j = 1 : TS_LICZBA_ZBIOROW
        % alias h_2_f
        y_k = h_2_ts;
        e_k(j) = y_zad - y_k;
        delta_u_k = sterowanieDMC(DMC_lokalne(j).K, y_zad, y_k, DMC_lokalne(j).MP, DMC_lokalne(j).delta_u_p);
        DMC_lokalne(j).delta_u_p = przesunDeltaU(DMC_lokalne(j).D, DMC_lokalne(j).delta_u_p, delta_u_k);
        % obliczenie nowego sterowania na podstawie poprzedniego oraz `delty`
        DMC_lokalne(j).u_k = DMC_lokalne(j).u_k + DMC_lokalne(j).delta_u_p(1); 
    end
    delta_u_k_ts = 0;
    for j = 1 : TS_LICZBA_ZBIOROW
       delta_u_k_ts = delta_u_k_ts + w(j) * DMC_lokalne(j).delta_u_p(1);
    end
    delta_u_k_ts = ograniczeniePrzyrostuSterowania(delta_u_k_ts, DELTA_U_MAX);
    u_k_ts = u_k_ts + delta_u_k_ts;
    % ograniczenie wartosci sterowania
    u_k_ts = ograniczenieSterowania(u_k_ts, U_MAX);
    % uwzględnienie TAU układu
    F_1_in_ts = zastosujOpoznienieWymuszeniaUkladu(F_1_in_ts, i, u_k_ts, TP);
    
    %% Dane do wykresów oraz aktualizacja wysokości słupa cieczy
    % macierze zmieniające swój rozmiar w kolejnych iteracjach
    V_1_wektor(i) = V_1;
    V_1_lin_wektor(i) = V_1_lin;
    h_1_wektor(i) = h_1;
    h_1_lin_wektor(i) = h_1_lin;
    V_2_wektor(i) = V_2;
    V_2_lin_wektor(i) = V_2_lin;
    h_2_wektor(i) = h_2;
    h_2_lin_wektor(i) = y_k_lin;
    V_1_ts_wektor(i) = V_1_ts;
    h_1_ts_wektor(i) = h_1_ts;
    V_2_ts_wektor(i) = V_2_ts;
    h_2_ts_wektor(i) = h_2_ts;        
    czas(i) = (i) * TP;
end

% okrojenie wektorow (zawierajacych niewykorzystanych x sterowan)
F_1_in_lin = F_1_in_lin(1:length(czas));
F_1_in_ts = F_1_in_ts(1:length(czas));


%% Wysokość słupa wody w zbiorniku nr 2
figure;
title('DMC - wysokość słupa wody w zbiorniku nr 2')
plot(czas, h_2_lin_wektor, 'r');
hold on
plot(czas, y_zad_wektor, 'g--');
hold on
% yyaxis right % sterowania na osi po prawej
plot(czas, F_1_in_ts, 'r--');
grid on
plot(czas, F_1_in_lin, 'm--');
grid on
ylabel('Dopływ [cm^3/s]');
legend('h_2 lin', 'y_{zad}', 'F_1', 'Location', 'best', 'NumColumns', 2);

%% Wysokość słupa wody w zbiorniku nr 2 (bez sterowań)
figure;
title('DMC - wysokość słupa wody w zbiorniku nr 2')
plot(czas, h_2_lin_wektor, 'r');
hold on
plot(czas, h_2_ts_wektor, 'k--');
hold on
plot(czas, y_zad_wektor, 'g--');
grid on
legend('h_2 lin', 'y_{zad}', 'Location', 'best', 'NumColumns', 2);