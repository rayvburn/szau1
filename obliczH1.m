function h_1 = obliczH1(V_1)
    global A_1
    h_1 = V_1 / A_1;
end