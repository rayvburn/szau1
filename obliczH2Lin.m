function h_2_lin = obliczH2Lin(V_2_lin, V_2_lin0)
    global C_2
    h_2_lin = power(V_2_lin0 / C_2, 1/3) + 1 / (3*C_2) * power(V_2_lin0 / C_2, -2/3) * (V_2_lin - V_2_lin0);
end