function [F_1_in_zad, F_1_in] = przygotujWektorSterowanUkladuOtwartego(amplituda_start, amplituda_wymuszenia, moment_wymuszenia, opoznienie_ukladu, dlugosc_eksperymentu, TP)
    %% Dodatkowe obliczenia
    % liczba próbek
    N_PROBKI = dlugosc_eksperymentu / TP;
    % moment startu wymuszenia - skoku jednostkowego (w postaci numeru próbki)
    N_SKOK = moment_wymuszenia / TP;
    % moment rzeczywistego startu wymuszenia, uwzględniając opóźnienie układu
    % (przedstawiony w postaci numeru próbki)
    N_SKOK_TAU = N_SKOK + opoznienie_ukladu / TP;
    
    % Sprawdzanie poprawności parametrów eksperymentu
    if moment_wymuszenia >= dlugosc_eksperymentu
        disp("Zmniejsz wartość momentu wymuszenia - inaczej sterowanie nie wystąpi");
        return
    elseif (dlugosc_eksperymentu - moment_wymuszenia) < opoznienie_ukladu 
        disp("Skok jednostkowy wywołany za późno - opóźnienie układu nie pozwoli sprawdzić odpowiedzi obiektu");
        return
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Wektor wymuszeń w czasie
    % w próbce o numerze `N_SKOK` wymuszenie
    % przechodzi z wartości 0 do 1
    %
    % F_1_in
    F_1_in = zeros(1, N_PROBKI);
    % wypełnienie wektora wymuszeń stałą wartością podaną w poleceniu
    F_1_in(:) = amplituda_start;
    F_1_in_zad = F_1_in;
    % wypełnienie wektora wymuszeń - skok jednostkowy żądany w momencie
    % `N_SKOK` przesunięty na podstawie wiedzy o opóźnieniu sterowania `TAU`
    F_1_in_zad(1, N_SKOK : N_PROBKI+1) = amplituda_wymuszenia;
    F_1_in(1, N_SKOK_TAU : N_PROBKI+1) = amplituda_wymuszenia;
end