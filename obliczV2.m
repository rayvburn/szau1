function V_2 = obliczV2(h_2)
    global C_2
    V_2 = C_2 * h_2^3;
end