function [V_1, V_2, V_1_lin, V_1_lin0, V_2_lin, V_2_lin0, h_1_lin, h_1_lin0, h_2_lin, h_2_lin0] = ...
    inicjalizujZmienneProcesuTS(punkty_pracy, V_1_in, V_2_in, h_1_in, h_2_in)
%UNTITLED2 Summary of this function goes here
%   punkty_pracy: struktura zdefiniowana w `podzielNaZbioryRozmyte`
   
    for i = 1 : length(punkty_pracy)
        V_1(i) = V_1_in;
        V_2(i) = V_2_in;

        V_1_lin(i) = V_1_in;
        V_1_lin0(i) = obliczV1(punkty_pracy(i).h_1);
        V_2_lin(i) = V_2_in;
        V_2_lin0(i) = obliczV2(punkty_pracy(i).h_2);

        h_1_lin(i) = h_1_in;
        h_1_lin0(i) = punkty_pracy(i).h_1;
        h_2_lin(i) = h_2_in;
        h_2_lin0(i) = punkty_pracy(i).h_2;
    end
end

