function [V_1, V_2, V_1_lin, V_1_lin0, V_2_lin, V_2_lin0, h_1_lin, h_1_lin0, h_2_lin, h_2_lin0] = inicjalizujZmienneProcesu(h_1, h_2)
% wektor `wierszowy`
V_1 = obliczV1(h_1);
V_2 = obliczV2(h_2);

V_1_lin = V_1;
V_1_lin0 = V_1;
V_2_lin = V_2;
V_2_lin0 = V_2;

h_1_lin = h_1;
h_1_lin0 = h_1;
h_2_lin = h_2;
h_2_lin0 = h_2;
end