function [sterowanie] = obliczPID(pomiar, wartosc_zadana)
    global TP
    
    Kp = 15.0;
    Ki = 5.00;
    Kd = 0.00;
    
    uchyb = wartosc_zadana - pomiar;
    sterowanie = (Kp * uchyb) + (Ki * uchyb * TP) + (Kd * uchyb / TP);
    
    % wypompowanie wody ze zbiornika nie jest możliwe
    if sterowanie < 0
        sterowanie = 0;
    end
end