function [funkcja] = Wyznacz_wartosci_funkcji_przynaleznosci(ile,min,max,h2,nachylenie)

punkt_zmiany = zeros(ile-1,1);
funkcja = zeros(ile,1);
delta = (max-min)/ile;

for iter = 1:ile-1
    punkt_zmiany(iter) = min + delta*iter;
end

if (h2 <= punkt_zmiany(1)-nachylenie(1))
    funkcja(1) = 1;
elseif (h2 > punkt_zmiany(1)-nachylenie(1)) && (h2 < punkt_zmiany(1)+nachylenie(1))
    funkcja(1) = -h2/(2*nachylenie(1)) + punkt_zmiany(1)/(2*nachylenie(1)) + 0.5;
else
    funkcja(1) = 0;
end

if (ile > 2)
    for iter = 2:ile-1
        if (h2 <= punkt_zmiany(iter-1)-nachylenie(iter-1))
            funkcja(iter) = 0;
        elseif (h2 >= punkt_zmiany(iter)+nachylenie(iter))
            funkcja(iter) = 0;
        elseif (h2 > punkt_zmiany(iter-1)-nachylenie(iter-1)) && (h2 < punkt_zmiany(iter-1)+nachylenie(iter-1))
            funkcja(iter) = h2/(2*nachylenie(iter-1)) -punkt_zmiany(iter-1)/(2*nachylenie(iter-1)) + 0.5;
        elseif (h2 > punkt_zmiany(iter)-nachylenie(iter)) && (h2 < punkt_zmiany(iter)+nachylenie(iter))
            funkcja(iter) = -h2/(2*nachylenie(iter)) + punkt_zmiany(iter)/(2*nachylenie(iter)) + 0.5;
        else
            funkcja(iter) = 1;
        end
    end
end

if (h2 >= punkt_zmiany(ile-1)+nachylenie(ile-1))
    funkcja(ile) = 1;
elseif (h2 > punkt_zmiany(ile-1)-nachylenie(ile-1)) && (h2 < punkt_zmiany(ile-1)+nachylenie(ile-1))
    funkcja(ile) = h2/(2*nachylenie(ile-1)) - punkt_zmiany(ile-1)/(2*nachylenie(ile-1)) + 0.5;
else
    funkcja(ile) = 0;
end

end

