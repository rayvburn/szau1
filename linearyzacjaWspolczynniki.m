function [wspolczynniki] = linearyzacjaWspolczynniki(F_1_0, F_D_0, h_1_0, h_2_0)
global A_1
global ALPHA_1
global ALPHA_2
global C_2

%% pierwsze równanie
wolny_1a = (-ALPHA_1 * sqrt(h_1_0)) / A_1;
wolny_1b = (ALPHA_1 * h_1_0) / (2 * sqrt(h_1_0) * A_1);
wspolczynniki.wolny_1 = wolny_1a + wolny_1b;
wspolczynniki.F1_1 = -1 / A_1;
wspolczynniki.FD_1 = -1 / A_1;
wspolczynniki.h1_1 = -ALPHA_1 / (2 * sqrt(h_1_0) * A_1);

% %% drugie równanie
% % część mianownika
% mian_cz = 3 * C_2 * power(h_2_0, 2);
% wolny_2a = (ALPHA_1 * sqrt(h_1_0) - ALPHA_2 * sqrt(h_2_0)) / mian_cz;
% wolny_2b = (-ALPHA_1 * h_1_0) / (mian_cz * 2 * sqrt(h_1_0));
% wolny_2c = (ALPHA_2 * h_2_0) / (mian_cz * 2 * sqrt(h_2_0));
% % pochodna ilorazu 2 funkcji
% h2_skl1 = -ALPHA_2 / (6 * C_2 * power(h_2_0, 2) * sqrt(h_2_0));
% h2_skl2 = (2 * ALPHA_2 * sqrt(h_2_0)) / (9 * power(C_2, 2) * power(h_2_0, 5));
% wolny_2d = -h_2_0 * (h2_skl1 + h2_skl2) ;
% wspolczynniki.wolny_2 = wolny_2a + wolny_2b + wolny_2c + wolny_2d;
% wspolczynniki.h1_2 = ALPHA_1 / (mian_cz * 2 * sqrt(h_1_0));
% wspolczynniki.h2_2 = h2_skl1 + h2_skl2;

wolny_2a = (ALPHA_1 * sqrt(h_1_0) - ALPHA_2 * sqrt(h_2_0)) / (3 * C_2 * power(h_2_0, 2));
wspolczynniki.h1_2 = ALPHA_1 / (6 * C_2 * sqrt(h_1_0) * power(h_2_0, 2));
wolny_2b = (-ALPHA_1 * h_1_0) / (6 * C_2 * sqrt(h_1_0) * power(h_2_0, 2));
wolny_2c = ((1.5 * ALPHA_2 * C_2 * power(h_2_0, 5/2)) + (-6 * ALPHA_2 * C_2 * power(h_2_0, 7/2))) / (9 * power(C_2, 2) * power(h_2_0, 4));
wspolczynniki.h2_2 = ((1.5 * ALPHA_2 * C_2 * power(h_2_0, 3/2)) + (-6 * ALPHA_2 * C_2 * power(h_2_0, 5/2))) / (9 * power(C_2, 2) * power(h_2_0, 4));
wspolczynniki.wolny_2 = wolny_2a + wolny_2b + wolny_2c;
end