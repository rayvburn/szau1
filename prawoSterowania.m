function [u] = prawoSterowania(K1, y_zad, y_k, MP, delta_u)
% 2.50
y0 = ones(size(MP, 1), 1) * y_k + MP * delta_u;
% 2.54
u = K1 * (y_zad * ones(size(MP,1), 1) - y0);
end

