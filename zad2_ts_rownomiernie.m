%% Zadanie 2. Lokalne modele Takagi-Sugeno. 
% Porównianie ich z modelem nieliniowym oraz zlinearyzowanym.
clear all;

%% Stałe związane z eksperymentem
load('stale.mat');
%% Warunki początkowe (w punkcie pracy)
load('warunki_poczatkowe.mat');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Konfiguracja przebiegu eksperymentu
global TP
% okres próbkowania [s], uwaga: (TP <= TAU / 2)
TP = 50;
% długość eksperymentu [s]
DLUGOSC = TP * 3000;
% moment zaaplikowania na układ wymuszenia w postaci skoku [s];
% korekta w postaci TAU (opóźnienia układu), aby uniknąć niepoprawnych wartości
% wymuszeń na starcie
CZAS_WYMUSZENIA = TAU + (DLUGOSC * 0.1);
% amplituda wymuszenia - skoku [cm^3/s] (wartość nie ma znaczenia, kiedy
% wyświetlane są przebiegi dla regulatorów)
AMPLITUDA = PUNKT_PRACY_F_1 + 7;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Przygotowanie wektorów sterowań układu.
[F_1_in_zad, F_1_in] = przygotujWektorSterowanUkladuOtwartego(PUNKT_PRACY_F_1, ...
    AMPLITUDA, CZAS_WYMUSZENIA, TAU, DLUGOSC, TP);
F_D = PUNKT_PRACY_F_D;
N_PROBKI = size(F_1_in, 2);

%% Podział zmiennej `h_2` na kilka zbiorów rozmytych
TS_LICZBA_ZBIOROW = 5;
TS_ZAKRES_H2 = [PUNKT_PRACY_H_2 - 10; PUNKT_PRACY_H_2 + 10];
TS_NACHYLENIE_TRAP = 0.2 * ones(TS_LICZBA_ZBIOROW-1,1);
ts_punkty_pracy = podzielNaZbioryRozmyte(TS_LICZBA_ZBIOROW, TS_ZAKRES_H2(1), TS_ZAKRES_H2(2));

% Wyliczenie wartości początkowych do modelu zlinearyzowanego
[V_1, V_2, V_1_lin, V_1_lin0, V_2_lin, V_2_lin0, h_1_lin, h_1_lin0, h_2_lin, h_2_lin0] = inicjalizujZmienneProcesu(h_1, h_2);

% Wartości początkowe dla modelu wynikowego TS
[~, ~, V_1_ts, ~, V_2_ts, V_2_ts_0, h_1_ts, h_1_ts_0, h_2_ts, h_2_ts_0] = inicjalizujZmienneProcesu(h_1, h_2);

% Wyliczenie wartości początkowych dla modeli lokalnych Takagi Sugeno
[V_1_fuzzy, V_2_fuzzy, V_1_lin_fuzzy, V_1_lin0_fuzzy, V_2_lin_fuzzy, V_2_lin0_fuzzy, h_1_lin_fuzzy, h_1_lin0_fuzzy, h_2_lin_fuzzy, h_2_lin0_fuzzy] ...
    = inicjalizujZmienneProcesuTS(ts_punkty_pracy, V_1, V_2, h_1, h_2);

%% Symulacja działania układu - modelu nieliniowego oraz zlinearyzowanego
for i = 1 : N_PROBKI
    %% Wybór sterowania z uprzednio przygotowanego wektora
    % który uwzględnia opóźnienie układu)
    F_1 = F_1_in(1, i);
           
    %% Model nieliniowy
    [V_1, V_2, h_1, h_2] = obiektNieliniowy(V_1, F_1, F_D, h_1, V_2, h_2);

    %% Model zlinearyzowany
    [V_1_lin, V_2_lin, h_1_lin, h_2_lin] = obiektZlinearyzowany(V_1_lin, ...
        F_1, F_D, h_1_lin, h_1_lin0, V_2_lin, V_2_lin0, h_2_lin, h_2_lin0);
    
    %% Model TS
    [V_1_ts, V_2_ts, h_1_ts, h_2_ts] = obiektZlinearyzowany(V_1_ts, ...
        F_1, F_D, h_1_ts, h_1_ts_0, V_2_ts, V_2_ts_0, h_2_ts, h_2_ts_0);

    %% Modele lokalne Takagi Sugeno
    w = Wyznacz_wartosci_funkcji_przynaleznosci(TS_LICZBA_ZBIOROW, TS_ZAKRES_H2(1), TS_ZAKRES_H2(2), h_2_ts, TS_NACHYLENIE_TRAP);
    for j = 1 : TS_LICZBA_ZBIOROW
        [V_1_lin_fuzzy(j), V_2_lin_fuzzy(j), h_1_lin_fuzzy(j), h_2_lin_fuzzy(j)] = obiektZlinearyzowany(V_1_lin_fuzzy(j), ...
            F_1, F_D, h_1_lin_fuzzy(j), h_1_lin0_fuzzy(j), V_2_lin_fuzzy(j), V_2_lin0_fuzzy(j), h_2_lin_fuzzy(j), h_2_lin0_fuzzy(j));
    end
    V_1_suma_wazona = 0; V_2_suma_wazona = 0;
    h_1_suma_wazona = 0; h_2_suma_wazona = 0;
    h_1_0_suma_wazona = 0; V_2_0_suma_wazona = 0; h_2_0_suma_wazona = 0;
    for j = 1 : TS_LICZBA_ZBIOROW
       V_1_suma_wazona = V_1_suma_wazona + w(j) * V_1_lin_fuzzy(j);
       V_2_suma_wazona = V_2_suma_wazona + w(j) * V_2_lin_fuzzy(j);
       h_1_suma_wazona = h_1_suma_wazona + w(j) * h_1_lin_fuzzy(j);
       h_2_suma_wazona = h_2_suma_wazona + w(j) * h_2_lin_fuzzy(j); 
       h_1_0_suma_wazona = h_1_0_suma_wazona + w(j) * h_1_lin0_fuzzy(j);
       V_2_0_suma_wazona = V_2_0_suma_wazona + w(j) * V_2_lin0_fuzzy(j);
       h_2_0_suma_wazona = h_2_0_suma_wazona + w(j) * h_2_lin0_fuzzy(j);
    end
    % TODO: na wszelki wypadek normalizacja wag
    V_1_ts = V_1_suma_wazona;
    V_2_ts = V_2_suma_wazona;
    h_1_ts = h_1_suma_wazona;
    h_2_ts = h_2_suma_wazona;
    h_1_ts_0 = h_1_0_suma_wazona;
    V_2_ts_0 = V_2_0_suma_wazona;
    h_2_ts_0 = h_2_0_suma_wazona;

    %% Dane do wykresów oraz aktualizacja wysokości słupa cieczy
    % macierze zmieniające swój rozmiar w kolejnych iteracjach
    V_1_wektor(i) = V_1;
    V_1_lin_wektor(i) = V_1_lin;
    h_1_wektor(i) = h_1;
    h_1_lin_wektor(i) = h_1_lin;
    V_2_wektor(i) = V_2;
    V_2_lin_wektor(i) = V_2_lin;
    h_2_wektor(i) = h_2;
    h_2_lin_wektor(i) = h_2_lin;
    h_2_ts_wektor(i) = h_2_ts;
    w_wektor{i} = w;
    czas(i) = (i-1) * TP;
end

mkdir('zad2_ts_rownomiernie');
%% Przebiegi sygnałów
figure;
title({'Wykres objetosci cieczy w zbiorniku od czasu';['dla zmiany F_1 o ',num2str(AMPLITUDA - PUNKT_PRACY_F_1)]})
plot(czas,h_1_wektor, 'g')
hold on
plot(czas, h_1_lin_wektor, 'm')
hold on
plot(czas, h_2_wektor, 'b')
hold on
plot(czas, h_2_lin_wektor, 'k')
ylabel('Wysokość poziomu cieczy [cm]')
yyaxis right
hold on
plot(czas, F_1_in_zad, ['--', 'r'])
hold on
plot(czas, F_1_in + F_D, ['-', 'r'])
xlabel('czas [s]');
ylabel('Dopływ [cm^3/s]');
grid on
legend('h_1', 'h_1_{lin}', 'h_2', 'h_2_{lin}', ...
    'F_{1in}_{zad}', 'Suma dopływów', ...
    'Location', 'east', 'NumColumns', 3);
mkdir('zad1_lin');
saveas(gcf, [pwd '/zad2_ts_rownomiernie/ts' num2str(TS_LICZBA_ZBIOROW) 'lokalnych_wysokosci-doplywy.png']);

%% Objętość zbiorników
figure;
title('Objętość zbiorników nr 1 i nr 2');
plot(czas, V_1_wektor, 'g');
hold on
plot(czas, V_1_lin_wektor, 'b--');
hold on
plot(czas, V_2_wektor, 'm');
hold on
plot(czas, V_2_lin_wektor, 'r--');
grid on
ylabel('Objętość [cm^3]')
legend('V_1 n-liniowy', 'V_1 zlinearyzowany', ...
    'V_2 n-liniowy', 'V_2 zlinearyzowany', ...
    'Location', 'east', 'NumColumns', 2);
saveas(gcf, [pwd '/zad2_ts_rownomiernie/ts' num2str(TS_LICZBA_ZBIOROW) 'lokalnych_objetosci.png']);

%% Wysokość słupa wody w zbiorniku nr 2
figure;
title('Wysokość słupa wody w zbiorniku nr 2')
plot(czas, h_2_wektor, 'b');
hold on
plot(czas, h_2_lin_wektor, 'r');
hold on
plot(czas, h_2_ts_wektor, 'g');
ylabel('Wysokość wody [cm]')
yyaxis right
hold on
plot(czas, F_1_in + F_D, 'm--');
grid on
ylabel('Dopływ [cm^3/s]');
legend('h2 model nieliniowy', 'h2 model zlinearyzowany', 'h2 model ts', ...
    'Suma dopływów',  'Location', 'east', 'NumColumns', 2);
saveas(gcf, [pwd '/zad2_ts_rownomiernie/ts' num2str(TS_LICZBA_ZBIOROW) 'lokalnych.png']);

%% Bartek: do odp. skokowej
% save("punkty_pracy_TS.mat", 'ts_punkty_pracy', 'TP', '-nocompression');