clear all;

%% Stałe związane z eksperymentem
load('stale.mat');
%% Warunki początkowe (w punkcie pracy)
load('warunki_poczatkowe.mat');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Konfiguracja przebiegu eksperymentu
global TP
% okres próbkowania [s], uwaga: (TP <= TAU / 2)
TP = 50;
% długość eksperymentu [s]
DLUGOSC = TP * 5000;
% moment zaaplikowania na układ wymuszenia w postaci skoku [s];
% korekta w postaci TAU (opóźnienia układu), aby uniknąć niepoprawnych wartości
% wymuszeń na starcie
CZAS_WYMUSZENIA = TAU + (DLUGOSC * 0.1);
AMPLITUDY = [   PUNKT_PRACY_F_1;
                PUNKT_PRACY_F_1 - 10;
                PUNKT_PRACY_F_1 + 10;
                PUNKT_PRACY_F_1 - 50;
                PUNKT_PRACY_F_1 + 50    ];

%% Przygotowanie wektorów sterowań układu.
% 2 argument określa amplitudę
[F_1_in_zad,   F_1_in]   = przygotujWektorSterowanUkladuOtwartego(PUNKT_PRACY_F_1, AMPLITUDY(1), CZAS_WYMUSZENIA, TAU, DLUGOSC, TP);
[F_1_in_zad_2, F_1_in_2] = przygotujWektorSterowanUkladuOtwartego(PUNKT_PRACY_F_1, AMPLITUDY(2), CZAS_WYMUSZENIA, TAU, DLUGOSC, TP);
[F_1_in_zad_3, F_1_in_3] = przygotujWektorSterowanUkladuOtwartego(PUNKT_PRACY_F_1, AMPLITUDY(3), CZAS_WYMUSZENIA, TAU, DLUGOSC, TP);
[F_1_in_zad_4, F_1_in_4] = przygotujWektorSterowanUkladuOtwartego(PUNKT_PRACY_F_1, AMPLITUDY(4), CZAS_WYMUSZENIA, TAU, DLUGOSC, TP);
[F_1_in_zad_5, F_1_in_5] = przygotujWektorSterowanUkladuOtwartego(PUNKT_PRACY_F_1, AMPLITUDY(5), CZAS_WYMUSZENIA, TAU, DLUGOSC, TP);
F_D = PUNKT_PRACY_F_D;
N_PROBKI = size(F_1_in, 2);

%% Wykorzystując dotychczasowe informacje (stałe, warunki początkowe) 
%% możliwe jest wyznaczenie wartości objętości w zbiornikach nr 1 i 2. 
fprintf("Początkowe wysokości cieczy w zbiornikach to: h_1: %3.5f, h_2: %3.5f\n", h_1, h_2);
% Wartości wyjściowe licząc od 3-ciej dotyczą zmiennych wykorzystywanych 
% przy linearyzacji.
% zmienne *_lin0 są stałe przez cały czas trwania eksperymentu;
% można zignorować nieużywane wartości wyjściowe funkcji przez `~`
h_1_2 = h_1;
h_1_3 = h_1;
h_1_4 = h_1;
h_1_5 = h_1;

h_2_2 = h_2;
h_2_3 = h_2;
h_2_4 = h_2;
h_2_5 = h_2;

[V_1,   V_2,   V_1_lin,   V_1_lin0,   V_2_lin,   V_2_lin0,   h_1_lin,   h_1_lin0,   h_2_lin,   h_2_lin0]   = inicjalizujZmienneProcesu(h_1,   h_2);
[V_1_2, V_2_2, V_1_lin_2, V_1_lin0_2, V_2_lin_2, V_2_lin0_2, h_1_lin_2, h_1_lin0_2, h_2_lin_2, h_2_lin0_2] = inicjalizujZmienneProcesu(h_1_2, h_2_2);
[V_1_3, V_2_3, V_1_lin_3, V_1_lin0_3, V_2_lin_3, V_2_lin0_3, h_1_lin_3, h_1_lin0_3, h_2_lin_3, h_2_lin0_3] = inicjalizujZmienneProcesu(h_1_3, h_2_3);
[V_1_4, V_2_4, V_1_lin_4, V_1_lin0_4, V_2_lin_4, V_2_lin0_4, h_1_lin_4, h_1_lin0_4, h_2_lin_4, h_2_lin0_4] = inicjalizujZmienneProcesu(h_1_4, h_2_4);
[V_1_5, V_2_5, V_1_lin_5, V_1_lin0_5, V_2_lin_5, V_2_lin0_5, h_1_lin_5, h_1_lin0_5, h_2_lin_5, h_2_lin0_5] = inicjalizujZmienneProcesu(h_1_5, h_2_5);

%% Odpowiedź skokowa 
% znalezienie numeru próbki skojarzonej z początkiem wymuszenia w postaci
% skoku jednostkowego
PROBKA_SKOKU = -1;
for i = 1 : N_PROBKI
    if F_1_in_zad(i) ~= PUNKT_PRACY_F_1
        PROBKA_SKOKU = i;
        break;
    end
end
    
%% Symulacja działania układu - modelu nieliniowego oraz zlinearyzowanego
for i = 1 : N_PROBKI
    %% Wybór sterowania z uprzednio przygotowanego wektora
    % który uwzględnia opóźnienie układu)
    F_1   = F_1_in(1, i);
    F_1_2 = F_1_in_2(1, i);
    F_1_3 = F_1_in_3(1, i);
    F_1_4 = F_1_in_4(1, i);
    F_1_5 = F_1_in_5(1, i);
    
    %% Model nieliniowy
    [V_1,   V_2,   h_1,   h_2]   = obiektNieliniowy(V_1,   F_1,   F_D, h_1,   V_2,   h_2);
    [V_1_2, V_2_2, h_1_2, h_2_2] = obiektNieliniowy(V_1_2, F_1_2, F_D, h_1_2, V_2_2, h_2_2);
    [V_1_3, V_2_3, h_1_3, h_2_3] = obiektNieliniowy(V_1_3, F_1_3, F_D, h_1_3, V_2_3, h_2_3);
    [V_1_4, V_2_4, h_1_4, h_2_4] = obiektNieliniowy(V_1_4, F_1_4, F_D, h_1_4, V_2_4, h_2_4);
    [V_1_5, V_2_5, h_1_5, h_2_5] = obiektNieliniowy(V_1_5, F_1_5, F_D, h_1_5, V_2_5, h_2_5);

    %% Model zlinearyzowany
    [V_1_lin,   V_2_lin,   h_1_lin,   h_2_lin]   = obiektZlinearyzowany(V_1_lin,   F_1,   F_D, h_1_lin,   h_1_lin0,   V_2_lin,   V_2_lin0,   h_2_lin,   h_2_lin0);
    [V_1_lin_2, V_2_lin_2, h_1_lin_2, h_2_lin_2] = obiektZlinearyzowany(V_1_lin_2, F_1_2, F_D, h_1_lin_2, h_1_lin0_2, V_2_lin_2, V_2_lin0_2, h_2_lin_2, h_2_lin0_2);
    [V_1_lin_3, V_2_lin_3, h_1_lin_3, h_2_lin_3] = obiektZlinearyzowany(V_1_lin_3, F_1_3, F_D, h_1_lin_3, h_1_lin0_3, V_2_lin_3, V_2_lin0_3, h_2_lin_3, h_2_lin0_3);
    [V_1_lin_4, V_2_lin_4, h_1_lin_4, h_2_lin_4] = obiektZlinearyzowany(V_1_lin_4, F_1_4, F_D, h_1_lin_4, h_1_lin0_4, V_2_lin_4, V_2_lin0_4, h_2_lin_4, h_2_lin0_4);
    [V_1_lin_5, V_2_lin_5, h_1_lin_5, h_2_lin_5] = obiektZlinearyzowany(V_1_lin_5, F_1_5, F_D, h_1_lin_5, h_1_lin0_5, V_2_lin_5, V_2_lin0_5, h_2_lin_5, h_2_lin0_5);
    
    %% Dane do wykresów oraz aktualizacja wysokości słupa cieczy
    % macierze zmieniające swój rozmiar w kolejnych iteracjach
    V_1_wektor(i) = V_1;
    V_1_2_wektor(i) = V_1_2;
    V_1_3_wektor(i) = V_1_3;
    V_1_4_wektor(i) = V_1_4;
    V_1_5_wektor(i) = V_1_5;
    V_1_lin_wektor(i) = V_1_lin;
    V_1_lin_2_wektor(i) = V_1_lin_2;
    V_1_lin_3_wektor(i) = V_1_lin_3;
    V_1_lin_4_wektor(i) = V_1_lin_4;
    V_1_lin_5_wektor(i) = V_1_lin_5;
    h_1_wektor(i) = h_1;
    h_1_2_wektor(i) = h_1_2;
    h_1_3_wektor(i) = h_1_3;
    h_1_4_wektor(i) = h_1_4;
    h_1_5_wektor(i) = h_1_5;
    h_1_lin_wektor(i) = h_1_lin;
    h_1_lin_2_wektor(i) = h_1_lin_2;
    h_1_lin_3_wektor(i) = h_1_lin_3;
    h_1_lin_4_wektor(i) = h_1_lin_4;
    h_1_lin_5_wektor(i) = h_1_lin_5;
    V_2_wektor(i) = V_2;
    V_2_2_wektor(i) = V_2_2;
    V_2_3_wektor(i) = V_2_3;
    V_2_4_wektor(i) = V_2_4;
    V_2_5_wektor(i) = V_2_5;
    V_2_lin_wektor(i) = V_2_lin;
    V_2_lin_2_wektor(i) = V_2_lin_2;
    V_2_lin_3_wektor(i) = V_2_lin_3;
    V_2_lin_4_wektor(i) = V_2_lin_4;
    V_2_lin_5_wektor(i) = V_2_lin_5;
    h_2_wektor(i) = h_2;
    h_2_2_wektor(i) = h_2_2;
    h_2_3_wektor(i) = h_2_3;
    h_2_4_wektor(i) = h_2_4;
    h_2_5_wektor(i) = h_2_5;
    h_2_lin_wektor(i) = h_2_lin;
    h_2_lin_2_wektor(i) = h_2_lin_2;
    h_2_lin_3_wektor(i) = h_2_lin_3;
    h_2_lin_4_wektor(i) = h_2_lin_4;
    h_2_lin_5_wektor(i) = h_2_lin_5;
    czas(i) = (i-1) * TP;
end

%% Wysokość słupa wody w zbiorniku nr 2
plot(czas, h_2_wektor, 'k')
hold on
plot(czas, h_2_2_wektor, 'g') 
hold on
plot(czas, h_2_3_wektor, 'm')
hold on
plot(czas, h_2_4_wektor, 'r')
hold on
plot(czas, h_2_5_wektor, 'b')
hold on
plot(czas, h_2_lin_wektor, '--k')
hold on
plot(czas, h_2_lin_2_wektor, '--g')
hold on
plot(czas, h_2_lin_3_wektor, '--m')
hold on
plot(czas, h_2_lin_4_wektor, '--r')
hold on
plot(czas, h_2_lin_5_wektor, '--b')
xlabel('Czas [s]')
ylabel('Poziom cieczy w zbiorniku 2 [cm]')
legend( 'F1_{const}', ...
        'F1_{-10}', ...
        'F1_{+10}', ...
        'F1_{-50}', ...
        'F1_{+50}', ...
        'F1lin_{const}', ...
        'F1lin_{-10}', ...
        'F1lin_{+10}', ...
        'F1lin_{-50}', ...
        'F1lin_{+50}', ...
        'Location', 'east');

mkdir('zad1_porownanie_nlin_lin');
saveas(gcf, [pwd '/zad1_porownanie_nlin_lin/h2-f1.png']);