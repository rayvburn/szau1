function [delta_V_1, delta_V_2] = deltaLinWspolczynniki(h_1, h_2, wspolczynniki, krok, F_1, F_D)
dhdt(1) = wspolczynniki.wolny_1 + wspolczynniki.h1_1 * h_1 + wspolczynniki.F1_1 * F_1 + wspolczynniki.FD_1 * F_D;
dhdt(2) = wspolczynniki.wolny_2 + wspolczynniki.h1_2 * h_1 + wspolczynniki.h2_2 * h_2;

delta_V_1 = krok * (wspolczynniki.wolny_1 + wspolczynniki.h1_1 * calkowanie(h_1, dhdt(1), krok) + wspolczynniki.F1_1 * F_1 + wspolczynniki.FD_1 * F_D);
delta_V_2 = krok * (wspolczynniki.wolny_2 + wspolczynniki.h1_2 * calkowanie(h_1, dhdt(1), krok) + wspolczynniki.h2_2 * calkowanie(h_2, dhdt(2), krok));
end

function [x_k] = calkowanie(x_k_1, dx, T)
x_k = x_k_1 + 0.5 * T * dx;
end


