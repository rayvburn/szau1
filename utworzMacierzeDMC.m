% function [M,MP,K1] = utworzMacierzeDMC(S, N, Nu, D, lambda)
% M = utworzMacierzM(S, N, Nu);
% MP = utworzMacierzMP(S, D, N);
% K1 = obliczDMC_K1(M, Nu, lambda);
% end
function [M,MP,K1] = utworzMacierzeDMC(S, N, Nu, D, lambda)
M = utworzMacierzM(S, N, Nu);
MP = utworzMacierzMP(S, D, N);
[K1] = obliczDMC_K1(M, Nu, lambda);
end

function [M] = utworzMacierzM(S, N, Nu)
%     M = zeros(N, Nu);
%     for j = 1:Nu
%         for i = j:N
%             if i - j + 1 > size(S, 2)
%                 M(i, j) = S(end);
%             elseif i - j + 1 < 1
%                 M(i, j) = 0;
%             else
%                 M(i, j) = S(i - j + 1);
%             end
%         end
%     end
M=zeros(N,Nu);
for i=1:N
   for j=1:Nu
      if (i>=j)
         M(i,j)=S(i-j+1);
      end
   end
end
end

function [MP] = utworzMacierzMP(S, D, N)
%     MP = zeros(N, D-1);
%     for j = 1:D-1
%         for i = 1:N
%             if i + j > D
%                 MP(i, j) = S(end) - S(j);
%             else
%                 MP(i, j) = S(i+j) - S(j);
%             end
%         end
%     end
MP=zeros(N,D-1);
for i=1:N
   for j=1:D-1
      if i+j<=D
         MP(i,j)=S(i+j)-S(j);
      else
         MP(i,j)=S(D)-S(j);
      end    
   end
end
end

% function [K] = obliczDMC_K1(M, lambda)
% K = zeros(size(M'));
% K = (inv(M'*M+lambda*eye(size(M,2))))*M';
% K = K(1,:);
% end

function [K] = obliczDMC_K1(M, Nu, lambda)
I=eye(Nu);
K=((M'*M+lambda*I)^-1)*M';
K = K(1,:);
end

% function [Ku, Ke] = obliczDMC_K1(M, MP, Nu, lambda)
% I=eye(Nu);
% K=((M'*M+lambda*I)^-1)*M';
% Ku=K(1,:)*MP;
% Ke=sum(K(1,:));
% end