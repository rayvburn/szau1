function [u_out] = zastosujOpoznienieWymuszeniaUkladu(u, i_akt, wartosc, tp)
%ZASTOSUJOPOZNIENIEWYMUSZENIAUKLADU Summary of this function goes here
%   Detailed explanation goes here
    global TAU
    if mod(TAU, tp) ~= 0
        error('Wybierz TP podzielne bez reszty przez TAU - nie mogę przesunąć wymuszeń o liczbę całkowitą');
    end
    przesuniecie = TAU / tp;
    u(i_akt + przesuniecie) = wartosc;
    u_out = u;
end

