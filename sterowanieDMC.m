function [du] = sterowanieDMC(K1, y_zad, y_k, MP, delta_u)
% size(y_k)
% size(MP)
% size(delta_u)
% ones(size(MP,1),1) * y_k

%  size(MP)
%  size(delta_u)
% MP * delta_u'

%% 2.50
y_0 = ones(size(MP,1),1) * y_k + MP * delta_u';
% 2.54
du = K1 * (y_zad * ones(size(MP,1), 1) - y_0);
end

