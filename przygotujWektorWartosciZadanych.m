function [y_zad_wektor] = przygotujWektorWartosciZadanych(dlugosc, t, y_zad)
% `dlugosc` - dlugosc wynikowego wektora
% `t` to wektor wartości <0.0; 0.1> oznaczających % długości przebiegu, kiedy
% w.zadana ma być ustalona jako wiążąca
% `y_zad` to wektor wartosci zadanych

    n = length(t);
    if (length(t) + 1) ~= length(y_zad)
        error('Wektor w. zadanych musi być o 1 dłuższy od wektora podziału czasu');
    end
    if t(1) < 1e-1
        error('Pierwsza wartość oznacza koniec pierwszego przedziału, użyj > 0');
    end
    
    y_zad_wektor = zeros(dlugosc, 1);
    
    % znajdz indeksy startow w. zadanych
    starty = zeros(n, 1);
    for i = 1 : n
        starty(i) = round(t(i) * dlugosc);
    end
    
    y_zad_wektor(1:starty(1)) = y_zad(1);
    for i = 1 : (n-1)
        y_zad_wektor(starty(i):starty(i+1)) = y_zad(i+1);
    end
    y_zad_wektor(starty(n):end) = y_zad(n + 1);
    
%     global TAU    
%     % uwzgledniajac TAU
%     y_zad_wektor_in = y_zad_wektor;
%     for i = TAU : dlugosc
%         y_zad_wektor_in(i) 
%     end
end