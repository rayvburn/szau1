function [nowe_u] = ograniczenieSterowania(u, u_max)
if abs(u) > abs(u_max)
    nowe_u = u_max * sign(u);
else
    nowe_u = u;
end
end

