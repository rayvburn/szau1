clear all;

global A_1
global C_2
global ALPHA_1
global ALPHA_2
global TAU

% Pole przekroju prostokątnego zbiornika (nr 1) [cm^2]
A_1 = 720;
% Stała związana z kształtem stożkowego zbiornika (nr 2)
C_2 = 0.4;
% Stała związana z odpływem F2
ALPHA_1 = 12;
% Stała związana z odpływem F3
ALPHA_2 = 14;
% opóźnienie sterowania wartością `F_1` [s]
TAU = 300;

%% Punkt pracy
global PUNKT_PRACY_F_1
global PUNKT_PRACY_F_D
global PUNKT_PRACY_H_2

% dopływ sterowany (zbiornik nr 1) [cm^3/s]
PUNKT_PRACY_F_1 = 96;
% dopływ zakłócający (zbiornik nr 1) [cm^3/s]
PUNKT_PRACY_F_D = 33.5;
% wysokość cieczy w zbiorniku nr 2 [cm]
PUNKT_PRACY_H_2 = 85.5625;

save('stale.mat', '*', '-nocompression');