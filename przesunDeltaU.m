function [delta_u_p] = przesunDeltaU(D, delta_u_p, delta_u_k)
%UNTITLED `rejestr przesuwny`, przesunięcie próbek o 1 pozycję - najstarsza usuwana
%   Detailed explanation goes here
    for n = D-1:-1:2
       delta_u_p(n) = delta_u_p(n-1);
    end
    delta_u_p(1) = delta_u_k;
end

